﻿using ReportGenerator.Common.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.Common.ExcelViews
{
    public class ExcelTransactionDto
    {
        [Column(1)]
        [Required]
        public int OutAccount { get; set; }

        [Column(2)]
        [Required]
        public int InAccount { get; set; }

        [Column(3)]
        [Required]
        public decimal Count { get; set; }

        [Column(4)]
        [Required]
        public DateTime Date { get; set; }

        [Column(5)]
        [Required]
        public string Comment { get; set; }
    }
}
