﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.Common.Extensions
{
    /// <summary>
    /// The ListExtensions
    /// </summary>
    public static class ListExtensions
    {
        /// <summary>
        /// The SplitList
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">all items</param>
        /// <param name="nSize">size list that we will be saving in db</param>
        /// <returns></returns>
        public static IEnumerable<List<T>> SplitList<T>(this List<T> items, int nSize = 500)
        {
            for (int i = 0; i < items.Count; i += nSize)
            {
                yield return items.GetRange(i, Math.Min(nSize, items.Count - i));
            }
        }
    }
}
