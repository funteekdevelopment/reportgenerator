﻿using ReportGenerator.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.DataAccess.Context
{
    /// <summary>
    /// The ReportDbContext
    /// </summary>
    public class ReportDbContext : DbContext 
    {
        #region Property

        /// <summary>
        /// The Reports
        /// </summary>
        public DbSet<Transaction> Reports { get; set; }

        #endregion

        #region Constants

        private const string GetRevenue =
            @"select left(Date, 4) as Date, tReport.InAccount, Sum(tReport.Count) as Reveu from tReport  group by left(Date, 4), tReport.InAccount";

        private const string GetExpense =
            @"select left(Date, 4) as Date, tReport.OutAccount, Sum(tReport.Count) as Expense from tReport  group by left(Date, 4), tReport.OutAccount";

        #endregion

        #region Constructors

        /// <summary>
        /// Initializing ReportDbContext
        /// </summary>
        public ReportDbContext() : base("ReportDbContext")
        {

        }

        #endregion

        #region Protected Methods 

        /// <summary>
        /// Creating tables 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            InitializeEntity<Transaction>(modelBuilder, "tReport", "ReportId");
        }

        #endregion

        #region Private Methods 

        /// <summary>
        /// Initializing tables
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="modelBuilder"></param>
        /// <param name="tableName"></param>
        /// <param name="dataIDFieldName"></param>
        private void InitializeEntity<T>(DbModelBuilder modelBuilder, string tableName, string dataIDFieldName) where T : EntryBase
        {
            modelBuilder.Entity<T>().ToTable(tableName);
            modelBuilder.Entity<T>().HasKey(d => d.Id);
            modelBuilder.Entity<T>().Property(d => d.Id).HasColumnName(dataIDFieldName);
        }

        /// <summary>
        /// Add new entity in table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public void AddEntity<T>(T entity) where T : EntryBase
        {
            entity.CreateDate = DateTime.Now;
            Set<T>().Add(entity);
        }
        
        #endregion
    }
}
