namespace ReportGenerator.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_transactionDate_property : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tReport", "TransactionDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.tReport", "TransactionDate");
        }
    }
}
