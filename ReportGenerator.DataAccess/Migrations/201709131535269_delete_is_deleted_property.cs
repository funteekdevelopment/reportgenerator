namespace ReportGenerator.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class delete_is_deleted_property : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.tReport", "IsDeleted");
        }
        
        public override void Down()
        {
            AddColumn("dbo.tReport", "IsDeleted", c => c.Boolean(nullable: false));
        }
    }
}
