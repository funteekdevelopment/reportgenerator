namespace ReportGenerator.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_some_Properties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tReport", "CreateDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.tReport", "Date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.tReport", "Date", c => c.DateTime(nullable: false));
            DropColumn("dbo.tReport", "CreateDate");
        }
    }
}
