namespace ReportGenerator.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_some_changes_for_Report : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.tReport", "CreateDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.tReport", "CreateDate", c => c.DateTime(nullable: false));
        }
    }
}
