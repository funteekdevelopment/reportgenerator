namespace ReportGenerator.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_Report_entity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tReport",
                c => new
                    {
                        ReportId = c.Long(nullable: false, identity: true),
                        OutAccount = c.Int(nullable: false),
                        InAccount = c.Int(nullable: false),
                        Count = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Date = c.DateTime(nullable: false),
                        Comment = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ReportId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.tReport");
        }
    }
}
