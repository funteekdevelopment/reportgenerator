﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.DataAccess.DataObjects
{
    /// <summary>
    /// The acount
    /// </summary>
    public class Account
    {
        /// <summary>
        /// The AccountNumber
        /// </summary>
        public int AccountNumber { get; set; }

        /// <summary>
        /// The Date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The Count
        /// </summary>
        public decimal Count { get; set; }

        /// <summary>
        /// Dictionary that contains date(each moynt) as key and sum of this mounth
        /// </summary>
        public Dictionary<DateTime, decimal> SumsByDates { get; set; }

        public Account(int accountNumber)
        {
            this.AccountNumber = accountNumber;

            this.SumsByDates = new Dictionary<DateTime, decimal>();
        }

        public Account(int accountNumber, DateTime date, decimal count)
        {
            this.AccountNumber = accountNumber;
            this.Date = date;
            this.Count = count;
        }
    }
}
