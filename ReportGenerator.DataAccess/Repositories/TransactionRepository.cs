﻿using ReportGenerator.DataAccess.Context;
using ReportGenerator.DataAccess.Interfaces;
using ReportGenerator.DataAccess.Models;
using ReportGenerator.DataAccess.ViewModels;
using ReportGenerator.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.DataAccess.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        private const string DeleteTransactionsQuery = "delete from dbo.tReport";
        private const string GetRevenueQuery = "select Year(TransactionDate) as Year, month(TransactionDate) as Month, tReport.InAccount as AccountNumber, Sum(tReport.Count) as CountRevenue from tReport  group by Year(TransactionDate), month(TransactionDate), tReport.InAccount";
        private const string GetExpenseQuery = "select Year(TransactionDate) as Year, month(TransactionDate) as Month, tReport.OutAccount as AccountNumber, Sum(tReport.Count) as CountExpense from tReport  group by Year(TransactionDate), month(TransactionDate), tReport.OutAccount";


        /// <summary>
        /// Get accounts that have revenue 
        /// </summary>
        /// <returns></returns>
        public List<Revenue> GetRevenue()
        {
            var result = new List<Revenue>();

            using (var ctx = new ReportDbContext())
            {
                result = ctx.Database.SqlQuery<Revenue>(GetRevenueQuery).ToList<Revenue>();

                return result;
            }
        }

        /// <summary>
        /// Get accounts that have Expenses
        /// </summary>
        /// <returns></returns>
        public List<Expense> GetExpense()
        {
            var result = new List<Expense>();

            using (var ctx = new ReportDbContext())
            {
                result = ctx.Database.SqlQuery<Expense>(GetExpenseQuery).ToList<Expense>();

                return result;
            }
        }

        /// <summary>
        /// Insert Report item in db
        /// </summary>
        /// <param name="items"></param>
        public void InsertItems(IEnumerable<Transaction> items)
        {
            var lists = items.Where(item => item.TransactionDate != DateTime.MinValue
                                    && item.OutAccount != 0
                                    && item.InAccount != 0
                                    && item.Count != 0).ToList().SplitList();

            foreach(var list in lists)
            {
                using (ReportDbContext db = new ReportDbContext())
                {
                    foreach (var item in list)
                    {
                        db.AddEntity(item);
                    }
                    db.SaveChanges();
                }
            }
        }

        public void DeleteTransactions()
        {
            using (ReportDbContext db = new ReportDbContext())
            {
                db.Database.ExecuteSqlCommand(DeleteTransactionsQuery);
                db.SaveChanges();
            }
        }
    }
}
