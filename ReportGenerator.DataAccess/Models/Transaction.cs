﻿using ReportGenerator.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.DataAccess.Models
{
    /// <summary>
    /// The Report
    /// </summary>
    public class Transaction : EntryBase
    {
        #region Property

        /// <summary>
        /// The OutAccount
        /// </summary>
        public int OutAccount { get; set; }

        /// <summary>
        /// The InAccount
        /// </summary>
        public int InAccount { get; set; }

        /// <summary>
        /// The Count
        /// </summary>
        public decimal Count { get; set; }
        
        /// <summary>
        /// The Comment
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// The TransactionDate
        /// </summary>
        public DateTime TransactionDate { get; set; }

        #endregion
    }
}
