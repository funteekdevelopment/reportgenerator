﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.DataAccess.Models
{
    /// <summary>
    /// The EntryBase is base for the all entities in project
    /// </summary>
    public class EntryBase
    {
        #region Property

        /// <summary>
        /// The Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The CreateDate
        /// </summary>
        public DateTime CreateDate { get; set; }

        #endregion
    }
}
