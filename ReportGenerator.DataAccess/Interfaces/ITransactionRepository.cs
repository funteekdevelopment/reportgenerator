﻿using ReportGenerator.DataAccess.Models;
using ReportGenerator.DataAccess.Repositories;
using ReportGenerator.DataAccess.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.DataAccess.Interfaces
{
    /// <summary>
    /// The IAccountRepository
    /// </summary>
    public interface ITransactionRepository
    {
        /// <summary>
        /// Get accounts that have revenue 
        /// </summary>
        /// <returns></returns>
        List<Revenue> GetRevenue();

        /// <summary>
        /// Get accounts that have Expenses
        /// </summary>
        /// <returns></returns>
        List<Expense> GetExpense();

        /// <summary>
        /// Insert Report item in db
        /// </summary>
        /// <param name="items"></param>
        void InsertItems(IEnumerable<Transaction> items);

        /// <summary>
        /// Daelete all transactions
        /// </summary>
        void DeleteTransactions();
    }
}
