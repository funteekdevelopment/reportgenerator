﻿using ReportGenerator.Common.ExcelViews;
using ReportGenerator.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.DataAccess.Converters
{
    public interface IExcelViewConverter
    {
        IEnumerable<Transaction> TransactionConvert(IEnumerable<ExcelTransactionDto> listTransaction);
    }
}
