﻿using ReportGenerator.Common.ExcelViews;
using ReportGenerator.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.DataAccess.Converters
{
    public class ExcelViewConverter : IExcelViewConverter
    {
        public IEnumerable<Transaction> TransactionConvert(IEnumerable<ExcelTransactionDto> list)
        {
            var result = new List<Transaction>();

            result.AddRange(list.Select(item => new Transaction
            {
                Comment = item.Comment,
                Count = item.Count,
                TransactionDate = item.Date,
                InAccount = item.InAccount,
                OutAccount = item.OutAccount
            }));

            return result;
        }
    }
}
