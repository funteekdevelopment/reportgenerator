﻿using ReportGenerator.DataAccess.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportGenerator.BusinessLogic.ViewModels
{
    /// <summary>
    /// The ReportViewModel
    /// </summary>
    public class ReportViewModel
    {
        /// <summary>
        /// The AllDates contains all dates when we have transactions
        /// </summary>
        public List<DateTime> AllDates { get; set; }

        /// <summary>
        /// Dictionary for saving acounts
        /// </summary>
        public Dictionary<int, Account> Accounts { get; set; }

        /// <summary>
        /// List for saving Acounts
        /// </summary>
        public List<Account> AccountsList { get; set; }

        public ReportViewModel(List<DateTime> allDates, Dictionary<int, Account> acounts, List<Account> accountsList)
        {
            this.AllDates = allDates;
            this.Accounts = acounts;
            this.AccountsList = accountsList;
        }
    }
}