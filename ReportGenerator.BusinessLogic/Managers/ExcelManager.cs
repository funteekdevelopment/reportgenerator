﻿using ReportGenerator.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReportGenerator.Common.ExcelViews;
using System.IO;
using OfficeOpenXml;
using ReportGenerator.Common.Extensions;

namespace ReportGenerator.BusinessLogic.Managers
{
    public class ExcelManager : IExcelManager
    {
        public IEnumerable<ExcelTransactionDto> GetExcelTransactionObjects(Stream stream)
        {
            ExcelPackage package = new ExcelPackage(stream);

            int numberWorksheets = 1;

            ExcelWorksheet workSheet = package.Workbook.Worksheets[numberWorksheets];

            IEnumerable<ExcelTransactionDto> allTransactions = workSheet.ConvertSheetToObjects<ExcelTransactionDto>().ToList();

            return allTransactions;
        }
    }
}
