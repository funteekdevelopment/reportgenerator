﻿using OfficeOpenXml;
using ReportGenerator.Common.ExcelViews;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ReportGenerator.Common.Extensions;
using ReportGenerator.DataAccess.Interfaces;
using ReportGenerator.DataAccess.Repositories;
using ReportGenerator.DataAccess.Converters;
using ReportGenerator.DataAccess.Models;
using ReportGenerator.BusinessLogic.Interfaces;
using ReportGenerator.BusinessLogic.ViewModels;
using ReportGenerator.DataAccess.DataObjects;
using ReportGenerator.DataAccess.ViewModels;

namespace ReportGenerator.BusinessLogic.Managers
{
    public class TransactionManager : ITransactionManager
    {
        #region Fields

        /// <summary>
        /// The _transactionRepository
        /// </summary>
        private ITransactionRepository _transactionRepository;

        /// <summary>
        /// The _excelConverter
        /// </summary>
        private IExcelViewConverter _excelConverter;

        /// <summary>
        /// The _excelManager
        /// </summary>
        private IExcelManager _excelManager;

        #endregion

        #region Private Property

        /// <summary>
        /// The transactionRepository
        /// </summary>
        private ITransactionRepository transactionRepository
        {
            get
            {
                if (this._transactionRepository == null) this._transactionRepository = new TransactionRepository();

                return this._transactionRepository;
            }
        }

        /// <summary>
        /// The excelConverter
        /// </summary>
        private IExcelViewConverter excelConverter
        {
            get
            {
                if (this._excelConverter == null) this._excelConverter = new ExcelViewConverter();

                return this._excelConverter;
            }
        }

        /// <summary>
        /// The excelConverter
        /// </summary>
        private IExcelManager excelManager
        {
            get
            {
                if (this._excelManager == null) this._excelManager = new ExcelManager();

                return this._excelManager;
            }
        }

        #endregion

        public TransactionManager() { }

        public TransactionManager(ITransactionRepository transactionRepository, IExcelViewConverter excelConverter, IExcelManager excelManager)
        {
            this._transactionRepository = transactionRepository;
            this._excelConverter = excelConverter;
            this._excelManager = excelManager;
        }

        public void ImportTransaction(HttpPostedFileBase upload)
        {
            var t = Path.GetExtension(upload.FileName);

            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                IEnumerable<ExcelTransactionDto> allTransactions = this.excelManager.GetExcelTransactionObjects(upload.InputStream);

                IEnumerable<Transaction> insertItems = this.excelConverter.TransactionConvert(allTransactions);

                this.transactionRepository.DeleteTransactions();

                this.transactionRepository.InsertItems(insertItems);
            }
            else
            {
                throw new ArgumentException("File does not match");
            }
        }

        public ReportViewModel GetTransaction()
        {
            var dictionary = new Dictionary<int, Account>();

            var allDates = new List<DateTime>();
            
            var listRevenue = this.transactionRepository.GetRevenue();

            var listExpense = this.transactionRepository.GetExpense();

            // calculate revenue
            foreach (var item in listRevenue)
            {
                var date = new DateTime(item.Year, item.Month, DateTime.DaysInMonth(item.Year, item.Month));

                if (!dictionary.ContainsKey(item.AccountNumber))
                {
                    dictionary[item.AccountNumber] = new Account(item.AccountNumber);
                    dictionary[item.AccountNumber].SumsByDates.Add(date, item.CountRevenue);
                    if (!allDates.Contains(date)) allDates.Add(date);
                }
                else if (!dictionary[item.AccountNumber].SumsByDates.ContainsKey(date))
                {
                    dictionary[item.AccountNumber].SumsByDates.Add(date, item.CountRevenue);
                    if (!allDates.Contains(date)) allDates.Add(date);
                }
                else
                {
                    dictionary[item.AccountNumber].SumsByDates[date] = dictionary[item.AccountNumber].SumsByDates[date] + item.CountRevenue;
                    if (!allDates.Contains(date)) allDates.Add(date);
                }
            }

            // calculate expense
            foreach (var item in listExpense)
            {
                var date = new DateTime(item.Year, item.Month, DateTime.DaysInMonth(item.Year, item.Month));

                if (!dictionary.ContainsKey(item.AccountNumber))
                {
                    dictionary[item.AccountNumber] = new Account(item.AccountNumber);
                    dictionary[item.AccountNumber].SumsByDates.Add(date, -item.CountExpense);
                    if (!allDates.Contains(date)) allDates.Add(date);
                }
                else if (!dictionary[item.AccountNumber].SumsByDates.ContainsKey(date))
                {
                    dictionary[item.AccountNumber].SumsByDates.Add(date, -item.CountExpense);
                    if (!allDates.Contains(date)) allDates.Add(date);
                }
                else
                {
                    dictionary[item.AccountNumber].SumsByDates[date] = dictionary[item.AccountNumber].SumsByDates[date] - item.CountExpense;
                    if (!allDates.Contains(date)) allDates.Add(date);
                }
            }
            
            allDates.Sort((a, b) => a.CompareTo(b));

            var acountsList = this.ConvertDictionaryToList(dictionary);
            
            var result = new ReportViewModel(allDates, dictionary, acountsList);

            return result;
        }

        private List<Account> ConvertDictionaryToList(Dictionary<int, Account> dictionary)
        {
            var result = new List<Account>();

            foreach (var item in dictionary.Values)
            {
                foreach (var d in item.SumsByDates.Keys)
                {
                    var acount = new Account(item.AccountNumber, d, item.SumsByDates[d]);

                    result.Add(acount);
                }
            }

            return result;
        }
    }
}
