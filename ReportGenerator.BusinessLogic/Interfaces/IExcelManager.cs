﻿using ReportGenerator.Common.ExcelViews;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.BusinessLogic.Interfaces
{
    public interface IExcelManager
    {
        IEnumerable<ExcelTransactionDto> GetExcelTransactionObjects(Stream stream);
    }
}
