﻿using ReportGenerator.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ReportGenerator.BusinessLogic.Interfaces
{
    public interface ITransactionManager
    {
        void ImportTransaction(HttpPostedFileBase upload);

        ReportViewModel GetTransaction();
    }
}
