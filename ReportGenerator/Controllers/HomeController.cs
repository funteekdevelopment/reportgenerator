﻿using ReportGenerator.BusinessLogic.Interfaces;
using ReportGenerator.BusinessLogic.Managers;
using ReportGenerator.DataAccess.DataObjects;
using ReportGenerator.DataAccess.Models;
using ReportGenerator.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ReportGenerator.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// The _transactionManager
        /// </summary>
        private ITransactionManager _transactionManager;

        /// <summary>
        /// The transactionRepository
        /// </summary>
        private ITransactionManager transactionManager
        {
            get
            {
                if (this._transactionManager == null) this._transactionManager = new TransactionManager();

                return this._transactionManager;
            }
        }
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            
            return View();
        }

        [HttpPost]
        public ActionResult SaveFile()
        {
            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                    HttpPostedFileBase filebase = new HttpPostedFileWrapper(pic);
                    this.transactionManager.ImportTransaction(filebase);
                }
                
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch(Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }
        public ActionResult Report()
        {
            try
            {
                ViewBag.Title = "Home Page";

                var model = this.transactionManager.GetTransaction();

                return View(model);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }
        
    }
}
