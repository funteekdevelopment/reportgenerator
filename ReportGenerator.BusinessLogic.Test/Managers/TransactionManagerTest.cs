﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using ReportGenerator.BusinessLogic.Interfaces;
using ReportGenerator.BusinessLogic.Managers;
using ReportGenerator.Common.ExcelViews;
using ReportGenerator.DataAccess.Converters;
using ReportGenerator.DataAccess.Interfaces;
using ReportGenerator.DataAccess.Models;
using ReportGenerator.DataAccess.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ReportGenerator.BusinessLogic.Test.Managers
{
    [TestClass]
    public class TransactionManagerTest
    {
        /// <summary>
        /// The _transactionManager
        /// </summary>
        private ITransactionManager transactionManager;

        /// <summary>
        /// The mockTransactionRepository
        /// </summary>
        private Mock<ITransactionRepository> mockTransactionRepository;

        /// <summary>
        /// The mockExcelConverter
        /// </summary>
        private Mock<IExcelViewConverter> mockExcelConverter;

        /// <summary>
        /// The uploadedFile
        /// </summary>
        private Mock<HttpPostedFileBase> uploadedFile;

        /// <summary>
        /// The mockExcelManager
        /// </summary>
        private Mock<IExcelManager> mockExcelManager;

        [TestMethod]
        public void ImportTransactionTest()
        {
            // Arrange
            StreamWriter writer = File.CreateText("c:\\testfile.xlsx");
            writer.Close();
            FileStream fileStream = new FileStream("c:\\testfile.xlsx", FileMode.Open);
            this.uploadedFile = new Mock<HttpPostedFileBase>();

            this.uploadedFile.Setup(f => f.ContentLength).Returns(10);
            this.uploadedFile.Setup(f => f.FileName).Returns("testfile.xlsx");
            this.uploadedFile.Setup(f => f.InputStream).Returns(fileStream);

            this.mockTransactionRepository = new Mock<ITransactionRepository>();
            
            this.mockExcelManager = new Mock<IExcelManager>();
            List<ExcelTransactionDto> transactionObjects = new List<ExcelTransactionDto>();
            transactionObjects.Add(new ExcelTransactionDto { Comment = "test", Count = 100, Date = DateTime.Now, InAccount = 1, OutAccount = 2 });
            transactionObjects.Add(new ExcelTransactionDto { Comment = "test2", Count = 100, Date = DateTime.Now, InAccount = 1, OutAccount = 2 });
            transactionObjects.Add(new ExcelTransactionDto { Comment = "test3", Count = 100, Date = DateTime.Now, InAccount = 1, OutAccount = 2 });
            this.mockExcelManager.Setup(item => item.GetExcelTransactionObjects(It.IsAny<Stream>())).Returns(transactionObjects);

            this.mockExcelConverter = new Mock<IExcelViewConverter>();
            List<Transaction> listTransaction = new List<Transaction>();
            listTransaction.Add(new Transaction { Comment = "test", Count = 100, CreateDate = DateTime.Now, InAccount = 1, OutAccount = 2, TransactionDate = DateTime.Now });
            listTransaction.Add(new Transaction { Comment = "test2", Count = 100, CreateDate = DateTime.Now, InAccount = 1, OutAccount = 2, TransactionDate = DateTime.Now });
            listTransaction.Add(new Transaction { Comment = "test3", Count = 100, CreateDate = DateTime.Now, InAccount = 1, OutAccount = 2, TransactionDate = DateTime.Now });
            this.mockExcelConverter.Setup(item => item.TransactionConvert(It.IsAny<IEnumerable<ExcelTransactionDto>>())).Returns(listTransaction);

            this.transactionManager = new TransactionManager(this.mockTransactionRepository.Object, this.mockExcelConverter.Object, this.mockExcelManager.Object);

            // Act
            transactionManager.ImportTransaction(uploadedFile.Object);

            // Assert
            this.mockTransactionRepository.Verify(item => item.DeleteTransactions());
            this.mockTransactionRepository.Verify(item => item.InsertItems(It.IsAny<IEnumerable<Transaction>>()));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ImportTransactionFromWrongFileTest()
        {
            // Arrange
            this.uploadedFile = new Mock<HttpPostedFileBase>();
            
            this.uploadedFile.Setup(f => f.FileName).Returns("testfile.txt");

            this.mockTransactionRepository = new Mock<ITransactionRepository>();
            this.mockExcelManager = new Mock<IExcelManager>();
            this.mockExcelConverter = new Mock<IExcelViewConverter>();
            
            this.transactionManager = new TransactionManager(this.mockTransactionRepository.Object, this.mockExcelConverter.Object, this.mockExcelManager.Object);

            // Act
            transactionManager.ImportTransaction(uploadedFile.Object);
        }

        [TestMethod]
        public void GetTransactionTest()
        {
            // Arrange
            this.mockTransactionRepository = new Mock<ITransactionRepository>();
            this.mockExcelManager = new Mock<IExcelManager>();
            this.mockExcelConverter = new Mock<IExcelViewConverter>();
            var listRevenues = new List<Revenue>();
            listRevenues.Add(new Revenue { AccountNumber = 1, CountRevenue = 1, Month = DateTime.Now.Month, Year = DateTime.Now.Year }); 
            this.mockTransactionRepository.Setup(item => item.GetRevenue()).Returns(listRevenues);
            var listExpense = new List<Expense>();
            listExpense.Add(new Expense { AccountNumber = 1, CountExpense = 1, Month = DateTime.Now.Month, Year = DateTime.Now.Year });
            this.mockTransactionRepository.Setup(item => item.GetExpense()).Returns(listExpense);

            this.transactionManager = new TransactionManager(this.mockTransactionRepository.Object, this.mockExcelConverter.Object, this.mockExcelManager.Object);

            // Act
            var result = this.transactionManager.GetTransaction();

            // Assert
            NUnit.Framework.Assert.IsNotNull(result);
            NUnit.Framework.Assert.IsNotNull(result.AccountsList);
        }

        [TestMethod]
        public void GetTransactionWithoutRevenueTest()
        {
            // Arrange
            this.mockTransactionRepository = new Mock<ITransactionRepository>();
            this.mockExcelManager = new Mock<IExcelManager>();
            this.mockExcelConverter = new Mock<IExcelViewConverter>();
            var listRevenues = new List<Revenue>();
            this.mockTransactionRepository.Setup(item => item.GetRevenue()).Returns(listRevenues);
            var listExpense = new List<Expense>();
            listExpense.Add(new Expense { AccountNumber = 1, CountExpense = 1, Month = DateTime.Now.Month, Year = DateTime.Now.Year });
            this.mockTransactionRepository.Setup(item => item.GetExpense()).Returns(listExpense);

            this.transactionManager = new TransactionManager(this.mockTransactionRepository.Object, this.mockExcelConverter.Object, this.mockExcelManager.Object);

            // Act
            var result = this.transactionManager.GetTransaction();

            // Assert
            NUnit.Framework.Assert.IsNotNull(result);
            NUnit.Framework.Assert.IsNotNull(result.AccountsList);
            // Sum in all acounts shoul be less or same zero because we don't have revenue
            NUnit.Framework.Assert.IsTrue(result.AccountsList.FirstOrDefault().Count <= 0);
        }

        [TestMethod]
        public void GetTransactionWithoutExpenseTest()
        {
            // Arrange
            this.mockTransactionRepository = new Mock<ITransactionRepository>();
            this.mockExcelManager = new Mock<IExcelManager>();
            this.mockExcelConverter = new Mock<IExcelViewConverter>();
            var listRevenues = new List<Revenue>();
            listRevenues.Add(new Revenue { AccountNumber = 1, CountRevenue = 1, Month = DateTime.Now.Month, Year = DateTime.Now.Year });
            this.mockTransactionRepository.Setup(item => item.GetRevenue()).Returns(listRevenues);
            var listExpense = new List<Expense>();
            this.mockTransactionRepository.Setup(item => item.GetExpense()).Returns(listExpense);

            this.transactionManager = new TransactionManager(this.mockTransactionRepository.Object, this.mockExcelConverter.Object, this.mockExcelManager.Object);

            // Act
            var result = this.transactionManager.GetTransaction();

            // Assert
            NUnit.Framework.Assert.IsNotNull(result);
            NUnit.Framework.Assert.IsNotNull(result.AccountsList);
            // Sum in all acounts shoul be more or same zero because we don't have expense
            NUnit.Framework.Assert.IsTrue(result.AccountsList.FirstOrDefault().Count >= 0);
        }
    }
}
